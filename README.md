# PizzApp

> Version 0.9.0

## Intro

WebApp based on pizza management to learn the **Angular** framework.

This _frontend_ application was built using following technologies,

- Bulma
- Angular

### Context

- [x] Handling Input & Output
- [x] Handling Form & Events
- [x] Reactive Forms
- [x] Async Call (**RxJS**)
- [x] Angular Router
- [x] Directives
- [x] NgRx
- [x] Side Effects (**NgRx**)
- [x] _Keep Calm And Coding_
- [x] _All U Need Is Pizza_
- [x] Jest

## Process

Repository:

```
git clone -b 0.9.0 https://gitlab.com/dmnchzl/angular-pizzapp.git
```

Install:

```
npm install
```

Launch:

```
npm run start
```

Build:

```
npm run build
npx serve -s dist
```

Test:

```
npm run test:unit
```

## Docs

- **Bulma**: A Free, _Open-Source_ CSS Framework (Based On **Flexbox**)
  - [https://bulma.io/](https://bulma.io/)

- **Angular**: One Framework / Mobile & Desktop
  - [https://angular.io/](https://angular.io/)

- **NgRX**: Reactive State For Angular
  - [https://ngrx.io/](https://ngrx.io/)

- **Jest**: A Delightful JavaScript Testing Framework
  - [https://jestjs.io/](https://jestjs.io/)

- **Testing Lib**: Simple & Complete Testing Utilities
  - [https://testing-library.com/](https://testing-library.com/)

## License

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```

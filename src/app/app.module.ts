import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { RoutingModule } from './routing.module';

import { accountReducer as account, AccountEffects } from './store/account';
import { pizzasReducer as pizzas, PizzasEffect } from './store/pizzas';
import { storageMetaReducer as storage } from './store/storage.metareducer';

import { AppComponent } from './components/app.component';
import {
  CredentialsComponent,
  HomeComponent,
  LoginComponent,
  NavBarComponent,
  NotificationComponent,
  PizzaComponent,
  ProfileComponent,
  RegisterComponent,
  SettingsComponent
} from './components';

import { AwesomeErrorDirective } from './directives/awesome-error.directive';
import {
  ButtonDirective,
  ColumnDirective,
  NavBarDirective,
  NavMenuDirective,
  NotificationDirective,
  RowDirective,
} from './directives';

import { AccountService } from './services/account.service';
import { NotificationService } from './services/notification.service';
import { PizzasService } from './services/pizzas.service';

import { environment } from '../environments/environment.prod';

@NgModule({
  declarations: [
    AppComponent,
    CredentialsComponent,
    HomeComponent,
    LoginComponent,
    NavBarComponent,
    NotificationComponent,
    PizzaComponent,
    ProfileComponent,
    RegisterComponent,
    SettingsComponent,
    AwesomeErrorDirective,
    ButtonDirective,
    ColumnDirective,
    NavBarDirective,
    NavMenuDirective,
    NotificationDirective,
    RowDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RoutingModule,
    StoreModule.forRoot(
      { account, pizzas },
      {
        metaReducers: [storage(['account', 'pizzas'])] 
      }
    ),
    EffectsModule.forRoot([
      AccountEffects, 
      PizzasEffect
    ]),
    StoreDevtoolsModule.instrument({ logOnly: environment.production })
  ],
  providers: [
    AccountService, 
    NotificationService, 
    PizzasService
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

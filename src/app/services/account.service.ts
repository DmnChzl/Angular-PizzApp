import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import api from './api';
import { LoginAndPassword, TokenOnly, AccountWithPassword, Account, Profile, Credentials } from '../models/account.model';
import { environment } from '../../environments/environment';

@Injectable()
export class AccountService {

  constructor(private http: HttpClient) {}

  getHttpOptions(token?: string) {
    let options: { [name: string]: string | string[] } = {
      'Content-Type': 'application/json'
    };

    if (token) {
      options = {
        'Authorization': `Bearer ${token}`,
        ...options
      };
    }

    return {
      headers: new HttpHeaders(options)
    };
  }

  /**
   * Fetch Login Account
   *
   * @param {LoginAndPassword} account Account
   * @returns {Observable} Response | Error
   */
  fetchLoginAccount(account: LoginAndPassword): Observable<TokenOnly> {
    return this.http
      .post<TokenOnly>(
        environment.baseUrl + api.login(),
        JSON.stringify(account),
        this.getHttpOptions()
      )
      .pipe(catchError(() => throwError('Login Account Failure !')));
  }

  /**
   * Fetch Register Account
   *
   * @param {AccountWithPassword} account Account
   * @returns {Observable} Response | Error
   */
  fetchRegisterAccount(account: AccountWithPassword): Observable<TokenOnly> {
    return this.http
      .post<TokenOnly>(
        environment.baseUrl + api.register(),
        JSON.stringify(account),
        this.getHttpOptions()
      )
      .pipe(catchError(() => throwError('Register Account Failure !')));
  }

  /**
   * Fetch Read Account
   *
   * @param {String} token Token
   * @returns {Observable} Response | Error
   */
  fetchReadAccount(token: string): Observable<Account> {
    return this.http
      .get<Account>(
        environment.baseUrl + api.account(),
        this.getHttpOptions(token)
      )
      .pipe(catchError(() => throwError('Read Account Failure !')));
  }

  /**
   * Fetch Logout Account
   *
   * @param {String} token Token
   * @returns {Observable} Response | Error
   */
  fetchLogoutAccount(token: string): Observable<TokenOnly> {
    return this.http
      .get<TokenOnly>(
        environment.baseUrl + api.logout(),
        this.getHttpOptions(token)
      )
      .pipe(catchError(() => throwError('Logout Account Failure !')));
  }
  
  /**
   * Fetch Update Account
   *
   * @param {String} token Token
   * @param {Partial<Profile>} account Account
   * @returns {Observable} Response | Error
   */
  fetchUpdateAccount(token: string, account: Partial<Profile>): Observable<{ updatedId: string }> {
    return this.http
      .put<{ updatedId: string }>(
        environment.baseUrl + api.account(),
        JSON.stringify(account),
        this.getHttpOptions(token)
      )
      .pipe(catchError(() => throwError('Update Account Failure !')));
  }

  /**
   * Fetch Pswd Account
   *
   * @param {String} token Token
   * @param {Credentials} credentials Credentials
   * @returns {Observable} Response | Error
   */
  fetchPswdAccount(token: string, credentials: Credentials): Observable<{ updatedId: string }> {
    return this.http
      .put<{ updatedId: string }>(
        environment.baseUrl + api.pswd(),
        JSON.stringify(credentials),
        this.getHttpOptions(token)
      )
      .pipe(catchError(() => throwError('Pswd Account Failure !')));
  }

  /**
   * Fetch Delete Account
   *
   * @param {String} token Token
   * @returns {Observable} Response | Error
   */
  fetchDeleteAccount(token: string): Observable<{ deletedId: string }> {
    return this.http
      .delete<{ deletedId: string }>(
        environment.baseUrl + api.account(),
        this.getHttpOptions(token)
      )
      .pipe(catchError(() => throwError('Delete Account Failure !')));
  }
}

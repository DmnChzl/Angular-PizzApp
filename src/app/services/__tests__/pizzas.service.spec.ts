import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { PizzasService } from '../pizzas.service';
import api from '../api';
import { environment } from '../../../environments/environment';

describe('Pizza: Service', () => {
  let service: PizzasService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PizzasService]
    });
  });

  beforeEach(() => {
    const injector = getTestBed();

    service = injector.get(PizzasService);
    httpMock = injector.get(HttpTestingController);
  });
  
  afterEach(() => {
    httpMock.verify();
  });

  it("Should 'fetchAllPizzas' Returns Observable", async () => {
    service.fetchAllPizzas().subscribe(allPizzas => {
      expect(allPizzas).toHaveLength(1);
    });

    const request = httpMock.expectOne(environment.baseUrl + api.pizzas());

    request.flush([
      {
        id: 'ABCDEF123456',
        label: '4 Cheeses',
        items: ['Mozzarella', 'Goat Cheese', 'Reblochon', 'Gorgonzola'],
        price: 14.9
      }
    ]);
  });

  it("Should 'fetchAllPizzas' Returns Observable As Error", () => {
    service.fetchAllPizzas().subscribe(allPizzas => {
      expect(allPizzas).toHaveLength(0);
    });

    httpMock.expectOne(environment.baseUrl + api.pizzas()).error(new ErrorEvent('Bad Request'), {
      status: 400
    });
  });
});

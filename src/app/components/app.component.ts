import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { PizzasReducer } from '../models/pizzas.model';
import { allPizzas } from '../store/pizzas';

@Component({
  selector: 'app',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

  constructor(private store: Store<{ pizzas: PizzasReducer }>) {}

  ngOnInit() {
    console.log('Mounted !');

    this.store.dispatch(allPizzas('label'));
  }
}

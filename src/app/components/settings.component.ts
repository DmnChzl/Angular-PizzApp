import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { AccountReducer } from '../models/account.model';
import { selectToken } from '../store/account';
import { Router } from '@angular/router';

@Component({
  selector: 'settings',
  templateUrl: './settings.component.html'
})
export class SettingsComponent implements OnInit {

  constructor(private store$: Store<{ account: AccountReducer }>, private router: Router) {}

  ngOnInit() {
    this.store$.pipe(select(selectToken)).subscribe(token => {
      if (!token) {
        this.router.navigate(['/']);
      }
    });
  }
}

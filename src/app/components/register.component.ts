import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { AccountReducer } from '../models/account.model';
import { registerAccount, selectToken } from '../store/account';
import { validEmail, validNumber } from '../utils/custom.validator';

@Component({
  selector: 'register',
  templateUrl: './register.component.html'
})
export class RegisterComponent implements OnInit {
  registerForm = new FormGroup({
    login: new FormControl(''),
    password: new FormControl(''),
    repeatPassword: new FormControl(''),
    email: new FormControl('', validEmail),
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    gender: new FormControl(''),
    yearOld: new FormControl(0, validNumber(18, 99))
  });

  constructor(private store: Store<{ account: AccountReducer }>, private router: Router) {}

  ngOnInit() {
    this.store.pipe(select(selectToken)).subscribe(token => {
      if (!!token) {
        this.router.navigate(['/']);
      }
    })
  }

  getError(formControlName: string, formControlNameRef?: string) {
    const formControl = this.registerForm.controls[formControlName];

    if (formControlNameRef) {
      const formControlRef = this.registerForm.controls[formControlNameRef];

      return formControl.touched && (formControl.errors || formControl.value !== formControlRef.value);
    }

    return formControl.touched && formControl.errors;
  }

  handleSubmit() {
    const { login, password, email, firstName, lastName, gender, yearOld } = this.registerForm.value;

    this.store.dispatch(registerAccount({
      login,
      password,
      email,
      firstName,
      lastName,
      gender,
      yearOld
    }));
  }
}

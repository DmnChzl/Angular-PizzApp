import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { render, fireEvent, userEvent, waitFor } from '@testing-library/angular';
import { ProfileComponent } from '../profile.component';
import { NotificationComponent } from '../notification.component';
import { AwesomeErrorDirective } from '../../directives/awesome-error.directive';
import { ButtonDirective, ColumnDirective, NotificationDirective, RowDirective } from '../../directives';
import { NotificationService } from '../../services/notification.service';
import { accountReducer as account } from '../../store/account';
import { pizzasReducer as pizzas } from '../../store/pizzas';

describe('Profile: Component', () => {
  const initialState = {
    account: {
      email: 'rick.sanchez@pm.me',
      firstName: 'Rick',
      lastName: 'Sanchez',
      gender: 'M',
      yearOld: 70,
      token: 'ABCDEF123456'
    }
  };

  it('Should Component Renders', async () => {
    const { queryByDisplayValue } = await render(ProfileComponent, {
      declarations: [
        AwesomeErrorDirective,
        ButtonDirective,
        ColumnDirective,
        NotificationDirective,
        RowDirective,
        NotificationComponent
      ],
      imports: [
        ReactiveFormsModule,
        StoreModule.forRoot({ account, pizzas })
      ],
      providers: [
        NotificationService,
        provideMockStore({ initialState })
      ],
      routes: [
        {
          path: '',
          component: ProfileComponent
        }
      ]
    });

    expect(queryByDisplayValue('Rick')).toBeInTheDocument();
    expect(queryByDisplayValue('Sanchez')).toBeInTheDocument();
  });

  it('Should Form Validation Displays Error', async () => {
    const { getByDisplayValue, queryByText } = await render(ProfileComponent, {
      declarations: [
        AwesomeErrorDirective,
        ButtonDirective,
        ColumnDirective,
        NotificationDirective,
        RowDirective,
        NotificationComponent
      ],
      imports: [
        ReactiveFormsModule,
        StoreModule.forRoot({ account, pizzas })
      ],
      providers: [
        NotificationService,
        provideMockStore({ initialState })
      ],
      routes: [
        {
          path: '',
          component: ProfileComponent
        }
      ]
    });

    const input = getByDisplayValue('rick.sanchez@pm.me');

    fireEvent.blur(input);

    userEvent.type(input, '');

    await waitFor(() => {
      expect(queryByText('Email Error !')).toBeInTheDocument();
    });
  });
});

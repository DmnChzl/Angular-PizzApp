import { Component } from '@angular/core';
import { render, fireEvent } from '@testing-library/angular';
import { NotificationComponent } from '../notification.component';
import { NotificationDirective } from '../../directives';
import { NotificationService } from '../../services/notification.service';

@Component({
  template: `
    <div>
      <button (click)="handleClick()">Click Me</button>
      <notification></notification>
    </div>
  `
})
class HelloWorldComponent {
  constructor(private notificationService: NotificationService) {}

  handleClick() {
    this.notificationService.setMessage('Lorem Ipsum');
  }
}

describe('Notification: Component', () => {
  it('Should Component Renders', async () => {
    const { getByRole, queryByText } = await render(HelloWorldComponent, {
      declarations: [
        NotificationDirective,
        NotificationComponent
      ],
      providers: [
        NotificationService
      ]
    });

    const button = getByRole('button');

    fireEvent.click(button);

    expect(queryByText('Lorem Ipsum')).toBeInTheDocument();
  });
});

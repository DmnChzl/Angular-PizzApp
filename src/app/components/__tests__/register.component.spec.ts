import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { render, fireEvent, userEvent, waitFor } from '@testing-library/angular';
import { RegisterComponent } from '../register.component';
import { NotificationComponent } from '../notification.component';
import { AwesomeErrorDirective } from '../../directives/awesome-error.directive';
import { ButtonDirective, ColumnDirective, NotificationDirective, RowDirective } from '../../directives';
import { NotificationService } from '../../services/notification.service';
import { accountReducer as account } from '../../store/account';
import { pizzasReducer as pizzas } from '../../store/pizzas';

describe('Register: Component', () => {
  const initialState = {
    account: {
      token: null
    }
  };

  it('Should Component Renders', async () => {
    const { queryByPlaceholderText } = await render(RegisterComponent, {
      declarations: [
        AwesomeErrorDirective,
        ButtonDirective,
        ColumnDirective,
        NotificationDirective,
        RowDirective,
        NotificationComponent
      ],
      imports: [
        ReactiveFormsModule,
        StoreModule.forRoot({ account, pizzas })
      ],
      providers: [
        NotificationService,
        provideMockStore({ initialState })
      ],
      routes: [
        {
          path: '',
          component: RegisterComponent
        }
      ]
    });

    expect(queryByPlaceholderText('Rick')).toBeInTheDocument();
    expect(queryByPlaceholderText('Sanchez')).toBeInTheDocument();
  });

  it('Should Form Validation Displays Error', async () => {
    const { getByPlaceholderText, queryByText } = await render(RegisterComponent, {
      declarations: [
        AwesomeErrorDirective,
        ButtonDirective,
        ColumnDirective,
        NotificationDirective,
        RowDirective,
        NotificationComponent
      ],
      imports: [
        ReactiveFormsModule,
        StoreModule.forRoot({ account, pizzas })
      ],
      providers: [
        NotificationService,
        provideMockStore({ initialState })
      ],
      routes: [
        {
          path: '',
          component: RegisterComponent
        }
      ]
    });

    const email = getByPlaceholderText('rick.sanchez@pm.me');

    fireEvent.blur(email);

    userEvent.type(email, 'rick.sanchez');

    const yearOld = getByPlaceholderText('70');

    fireEvent.blur(yearOld);

    userEvent.type(yearOld, 100);

    await waitFor(() => {
      expect(queryByText('Email Error !')).toBeInTheDocument();
      expect(queryByText('Year Old Error !')).toBeInTheDocument();
    });
  });
});

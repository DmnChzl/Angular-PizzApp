import { FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { render, userEvent } from '@testing-library/angular';
import { HomeComponent } from '../home.component';
import { NotificationComponent } from '../notification.component';
import { ButtonDirective, ColumnDirective, NotificationDirective, RowDirective } from '../../directives';
import { NotificationService } from '../../services/notification.service';
import { accountReducer as account } from '../../store/account';
import { pizzasReducer as pizzas } from '../../store/pizzas';

describe('Home: Component', () => {
  const initialState = {
    pizzas: [
      {
        id: 'ABCDEF123456',
        label: '4 Cheeses',
        items: ['Mozzarella', 'Goat Cheese', 'Reblochon', 'Gorgonzola'],
        price: 14.9
      },
      {
        id: 'ABCDEF123457',
        label: 'Atlas',
        items: ['Mozzarella', 'Spicy Chicken', 'Chorizo', 'Marinated Zucchini', 'Ras-El-Hanout Spices'],
        price: 13.6
      },
      {
        id: 'ABCDEF123458',
        label: 'Barcelona',
        items: ['Mozzarella', 'Chorizo', 'Fried Onions', 'Peppers', 'Olives'],
        price: 13.3
      },
      {
        id: 'ABCDEF123459',
        label: 'Basque',
        items: ['Mozzarella', 'Spicy Chicken', 'Peppers', 'Candied Tomatoes'],
        price: 13.6
      }
    ]
  };

  it('Should Component Renders', async () => {
    const { queryByText } = await render(HomeComponent, {
      declarations: [
        ButtonDirective,
        ColumnDirective,
        NotificationDirective,
        RowDirective,
        NotificationComponent
      ],
      imports: [
        FormsModule,
        StoreModule.forRoot({ account, pizzas })
      ],
      providers: [
        NotificationService,
        provideMockStore({ initialState })
      ],
      routes: [
        {
          path: '',
          component: HomeComponent
        }
      ]
    });

    expect(queryByText('4 Cheeses')).toBeInTheDocument();
    expect(queryByText('Atlas')).toBeInTheDocument();
    expect(queryByText('Barcelona')).toBeInTheDocument();
    expect(queryByText('Basque')).toBeInTheDocument();
  });

  it('Should Filter Input Works', async () => {
    const { getByPlaceholderText, queryByText } = await render(HomeComponent, {
      declarations: [
        ButtonDirective,
        ColumnDirective,
        NotificationDirective,
        RowDirective,
        NotificationComponent
      ],
      imports: [
        FormsModule,
        StoreModule.forRoot({ account, pizzas })
      ],
      providers: [
        NotificationService,
        provideMockStore({ initialState })
      ],
      routes: [
        {
          path: '',
          component: HomeComponent
        }
      ]
    });

    const input = getByPlaceholderText('Filter');

    userEvent.type(input, 'Ba');

    expect(queryByText('4 Cheeses')).not.toBeInTheDocument();
    expect(queryByText('Atlas')).not.toBeInTheDocument();
    expect(queryByText('Barcelona')).toBeInTheDocument();
    expect(queryByText('Basque')).toBeInTheDocument();
  });
});

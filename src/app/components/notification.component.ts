import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../services/notification.service';

@Component({
  selector: 'notification',
  templateUrl: './notification.component.html'
})
export class NotificationComponent implements OnInit {
  message: string;
  color: string;

  constructor(private notificationService: NotificationService) {}

  ngOnInit() {
    this.notificationService.getMessage().subscribe(message => this.message = message);
    this.notificationService.getColor().subscribe(color => this.color = color);
  }

  onClick() {
    this.notificationService.setMessage('');
  }
}

import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { AccountReducer } from '../models/account.model';
import { selectLogin } from '../store/account';
import { Pizza, PizzasReducer } from '../models/pizzas.model';
import { selectPizzas, delPizza } from '../store/pizzas';
import { NotificationService } from '../services/notification.service';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  search = '';
  breakpoint = 4;
  login: string;
  allPizzas: Pizza[];

  constructor(private store: Store<{ account: AccountReducer, pizzas: PizzasReducer }>, private notificationService: NotificationService) {}

  ngOnInit() {
    this.store.pipe(select(selectLogin)).subscribe(login => this.login = login);
    this.store.pipe(select(selectPizzas)).subscribe(pizzas => this.allPizzas = pizzas);
  }

  isRoot() {
    return this.login === 'Pickle';
  }

  getMatrix() {
    const filter = this.search.toLowerCase();

    const filteredValues = this.allPizzas.filter(val => {
      const label = val['label'].toLowerCase();

      return label.includes(filter);
    });

    let outerVal = [];

    // Rows
    for (let i = 0; i < filteredValues.length; i += this.breakpoint) {
      let innerVal = [];

      // Cols
      for (let j = i; j < i + this.breakpoint; j++) {
        innerVal = [
          ...innerVal,
          filteredValues[j]
        ];
      }

      outerVal = [
        ...outerVal,
        innerVal
      ];
    }

    return outerVal;
  }

  handleClick(pizza) {
    this.store.dispatch(delPizza(pizza.id));
    this.notificationService.colorizeYellow();
    this.notificationService.setMessage(`${pizza.label} Removed !`);
  }
}

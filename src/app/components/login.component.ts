import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { AccountReducer } from '../models/account.model';
import { loginAccount, selectToken } from '../store/account';

@Component({
  selector: 'login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  loginForm = new FormGroup({
    login: new FormControl(''),
    password: new FormControl('')
  });

  constructor(private store: Store<{ account: AccountReducer }>, private router: Router) {}

  ngOnInit() {
    this.store.pipe(select(selectToken)).subscribe(token => {
      if (!!token) {
        this.router.navigate(['/']);
      }
    })
  }

  getError(formControlName: string) {
    const formControl = this.loginForm.controls[formControlName];

    return formControl.touched && formControl.errors;
  }

  handleSubmit() {
    const { login, password } = this.loginForm.value;

    this.store.dispatch(loginAccount({ login, password }));
  }
}

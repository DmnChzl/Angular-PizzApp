import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Store, select } from '@ngrx/store';
import { AccountReducer } from '../models/account.model';
import { updateAccount, selectAccount } from '../store/account';
import { validEmail, validNumber } from '../utils/custom.validator';

@Component({
  selector: 'profile',
  templateUrl: './profile.component.html'
})
export class ProfileComponent implements OnInit {
  hidden = true;

  profileForm: FormGroup;

  constructor(private store: Store<{ account: AccountReducer }>, private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.store.pipe(select(selectAccount)).subscribe(({ email, firstName, lastName, gender, yearOld }) => {
      this.profileForm = this.formBuilder.group({
        email: [email, validEmail],
        firstName,
        lastName,
        gender,
        yearOld: [yearOld, validNumber(18, 99)]
      })
    })
  }

  getError(formControlName: string, formControlNameRef?: string) {
    const formControl = this.profileForm.controls[formControlName];

    if (formControlNameRef) {
      const formControlRef = this.profileForm.controls[formControlNameRef];

      return formControl.touched && (formControl.errors || formControl.value !== formControlRef.value);
    }

    return formControl.touched && formControl.errors;
  }

  handleSubmit() {
    const { email, firstName, lastName, gender, yearOld } = this.profileForm.value;

    this.store.dispatch(updateAccount({
      email,
      firstName,
      lastName,
      gender,
      yearOld
    }));
  }
}

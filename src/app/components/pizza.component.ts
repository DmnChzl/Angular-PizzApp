import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Pizza } from '../models/pizzas.model';
import { ActivatedRoute } from '@angular/router';
import { selectPizzaById } from '../store/pizzas';

@Component({
  selector: 'pizza',
  templateUrl: './pizza.component.html'
})
export class PizzaComponent implements OnInit {
  hidden = false;
  pizza: Pizza;

  constructor(private store: Store<{ pizzas: Pizza[] }>, private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.store.pipe(select(state => selectPizzaById(state, params['id']))).subscribe(pizza => this.pizza = pizza);
    });
  }
}

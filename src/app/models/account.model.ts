interface LoginOnly {
  login: string;
}

interface PasswordOnly {
  password: string;
}

export interface Profile {
  email: string;
  firstName: string;
  lastName: string;
  gender: string;
  yearOld: number;
}

export type LoginAndPassword = LoginOnly & PasswordOnly;

export type Account = LoginOnly & Profile;

export type AccountWithPassword = Account & PasswordOnly;

type StringOrNull = string | null;

export interface TokenOnly {
  token: StringOrNull;
}

export interface Credentials {
  oldPswd: string;
  newPswd: string;
}

export type AccountReducer = Account & TokenOnly;

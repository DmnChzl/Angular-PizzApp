import { Directive, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  selector: '[myRow]'
})

export class RowDirective {
  constructor(ref: ElementRef, renderer: Renderer2) {
    renderer.addClass(ref.nativeElement, 'columns');
  }
}

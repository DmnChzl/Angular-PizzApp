import { Directive, OnInit, ElementRef, Renderer2, Input } from '@angular/core';

@Directive({
  selector: '[myButton]'
})

export class ButtonDirective implements OnInit {
  @Input() color: string;
  @Input() lighten: boolean;
  @Input() outlined: boolean;
  @Input() filled: boolean;

  constructor(private ref: ElementRef, private renderer: Renderer2) {
    renderer.addClass(ref.nativeElement, 'button');
  }

  ngOnInit() {
    if (this.color) {
      this.renderer.addClass(this.ref.nativeElement, 'is-' + this.color);
    }


    if (this.lighten) {
      this.renderer.addClass(this.ref.nativeElement, 'is-light');
    }


    if (this.outlined) {
      this.renderer.addClass(this.ref.nativeElement, 'is-outlined');
    }


    if (this.filled) {
      this.renderer.addClass(this.ref.nativeElement, 'is-fullwidth');
    }
  }
}

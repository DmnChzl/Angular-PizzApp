import { Directive, OnInit, ElementRef, Renderer2, Input } from '@angular/core';

@Directive({
  selector: '[myColumn]'
})

export class ColumnDirective implements OnInit {
  @Input() size: number;
  @Input() offset: number;

  constructor(private ref: ElementRef, private renderer: Renderer2) {
    renderer.addClass(ref.nativeElement, 'column');
  }

  ngOnInit() {
    if (this.size) {
      this.renderer.addClass(this.ref.nativeElement, 'is-' + this.size);
    }

    if (this.offset) {
      this.renderer.addClass(this.ref.nativeElement, 'is-offset-' + this.offset);
    }
  }
}

import { Directive, OnInit, OnChanges, ElementRef, Renderer2, Input } from '@angular/core';

@Directive({
  selector: '[myNavMenu]'
})

export class NavMenuDirective implements OnInit, OnChanges {
  @Input('myNavMenu') navMenu: string | string[];
  @Input() state: boolean;

  constructor(private ref: ElementRef, private renderer: Renderer2) {}

  toggleClass() {
    if (this.state) {
      this.renderer.addClass(this.ref.nativeElement, 'is-active');
    } else {
      this.renderer.removeClass(this.ref.nativeElement, 'is-active');
    }
  }

  ngOnInit() {
    if (Array.isArray(this.navMenu)) {
      this.navMenu.forEach(val => {
        this.renderer.addClass(this.ref.nativeElement, val);
      })
    } else {
      this.renderer.addClass(this.ref.nativeElement, this.navMenu);
    }

    this.toggleClass();
  }

  ngOnChanges() {
    this.toggleClass();
  }
}

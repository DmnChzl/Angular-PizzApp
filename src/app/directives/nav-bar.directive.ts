import { Directive, OnInit, ElementRef, Renderer2, Input } from '@angular/core';

@Directive({
  selector: '[myNavBar]'
})

export class NavBarDirective implements OnInit {
  @Input() elevated: boolean;
  @Input() spaced: boolean;

  constructor(private ref: ElementRef, private renderer: Renderer2) {
    renderer.addClass(ref.nativeElement, 'navbar');
  }

  ngOnInit() {
    if (this.elevated) {
      this.renderer.addClass(this.ref.nativeElement, 'has-shadow');
    }


    if (this.spaced) {
      this.renderer.addClass(this.ref.nativeElement, 'is-spaced');
    }
  }
}

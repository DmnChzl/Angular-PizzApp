import { Directive, OnInit, OnChanges, ElementRef, Renderer2, Input } from '@angular/core';

@Directive({
  selector: '[myAwesomeError]'
})

export class AwesomeErrorDirective implements OnInit, OnChanges {
  @Input('myAwesomeError') awesomeError: string | string[];
  @Input() state: boolean;

  constructor(private ref: ElementRef, private renderer: Renderer2) {}

  toggleClass() {
    if (this.state) {
      this.renderer.addClass(this.ref.nativeElement, 'is-danger');
    } else {
      this.renderer.removeClass(this.ref.nativeElement, 'is-danger');
    }
  }

  ngOnInit() {
    if (Array.isArray(this.awesomeError)) {
      this.awesomeError.forEach(val => {
        this.renderer.addClass(this.ref.nativeElement, val);
      })
    } else {
      this.renderer.addClass(this.ref.nativeElement, this.awesomeError);
    }

    this.toggleClass();
  }

  ngOnChanges() {
    this.toggleClass();
  }
}

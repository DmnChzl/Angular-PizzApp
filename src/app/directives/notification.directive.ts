import { Directive, OnInit, OnChanges, ElementRef, Renderer2, Input, HostBinding } from '@angular/core';

@Directive({
  selector: '[myNotification]'
})

export class NotificationDirective implements OnInit, OnChanges {
  @Input() color: string;

  @HostBinding('style.bottom')
  bottom = '1.5rem';

  @HostBinding('style.right')
  right = '1.5rem';

  constructor(private ref: ElementRef, private renderer: Renderer2) {
    renderer.addClass(ref.nativeElement, 'notification');

    // '@HostBinding' Alternative
    renderer.setStyle(ref.nativeElement, 'position', 'fixed');
    renderer.setStyle(ref.nativeElement, 'z-index', 5);
  }

  resetClasses() {
    ['danger', 'success', 'info', 'warning'].forEach(val => {
      this.renderer.removeClass(this.ref.nativeElement, 'is-' + val);
    });
  }

  setColor() {
    this.resetClasses();

    switch (this.color) {
      case 'red':
        this.renderer.addClass(this.ref.nativeElement, 'is-danger');
        break;
      case 'green':
        this.renderer.addClass(this.ref.nativeElement, 'is-success');
        break;
      case 'blue':
        this.renderer.addClass(this.ref.nativeElement, 'is-info');
        break;
      case 'yellow':
        this.renderer.addClass(this.ref.nativeElement, 'is-warning');
        break;

    }
  }

  ngOnInit() {
    this.setColor();
  }

  ngOnChanges() {
    this.setColor();
  }
}

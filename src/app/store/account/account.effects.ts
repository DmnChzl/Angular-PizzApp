import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of, concat, throwError } from 'rxjs';
import { map, concatMap, catchError, tap, mergeMap, withLatestFrom } from 'rxjs/operators';
import { loginAccount, readAccount, registerAccount, setToken, setAccount, resetAccount, logoutAccount, updateAccount, pswdAccount, deleteAccount } from './account.actions';
import { selectToken } from './account.selectors';
import { AccountReducer } from '../../models/account.model';
import { AccountService } from '../../services/account.service';
import { NotificationService } from '../../services/notification.service';
 
@Injectable()
export class AccountEffects {

  loginAccount$ = createEffect(() => {
    return this.actions.pipe(
      ofType(loginAccount),
      map(action => action.payload),
      concatMap(({ login, password }) => concat(
        of(setAccount({ login })),
        this.accountService.fetchLoginAccount({ login, password }).pipe(
          map(({ token }) => readAccount(token)),
          catchError((message: string) => {
            this.notificationService.colorizeRed();
            this.notificationService.setMessage(message);
            return of(resetAccount());
          })
        )
      ))
    )
  });

  readAccount$ = createEffect(() => {
    return this.actions.pipe(
      ofType(readAccount),
      map(action => action.payload),
      concatMap(token => concat(
        of(setToken(token)),
        this.accountService.fetchReadAccount(token).pipe(
          map(account => setAccount(account)),
          tap(() => this.router.navigate(['/'])),
          catchError((message: string) => {
            this.notificationService.colorizeRed();
            this.notificationService.setMessage(message);
            return of(resetAccount());
          })
        )
      ))
    )
  });

  registerAccount$ = createEffect(() => {
    return this.actions.pipe(
      ofType(registerAccount),
      map(action => action.payload),
      concatMap(({ password, ...account }) => concat(
        of(setAccount(account)),
        this.accountService.fetchRegisterAccount({ password, ...account }).pipe(
          map(({ token }) => setToken(token)),
          tap(() => this.router.navigate(['/'])),
          catchError((message: string) => {
            this.notificationService.colorizeRed();
            this.notificationService.setMessage(message);
            return of(resetAccount());
          })
        )
      ))
    )
  });

  logoutAccount$ = createEffect(() => {
    return this.actions.pipe(
      ofType(logoutAccount),
      withLatestFrom(this.store.select(selectToken)),
      map(([action, token]) => token),
      mergeMap(token => {
        return this.accountService.fetchLogoutAccount(token).pipe(
          map(() => resetAccount()),
          tap(() => this.router.navigate(['/'])),
          catchError(() => of(setToken(null)))
        )
      })
    )
  });

  updateAccount$ = createEffect(() => {
    return this.actions.pipe(
      ofType(updateAccount),
      withLatestFrom(this.store.select(selectToken)),
      map(([action, token]) => ({ ...action.payload, token })),
      mergeMap(({ token, ...account }) => {
        return this.accountService.fetchUpdateAccount(token, account).pipe(
          map(({ updatedId }) => {
            if (!updatedId) {
              throwError('No ID');
            }

            this.notificationService.colorizeGreen();
            this.notificationService.setMessage('Success !');
            return setAccount(account);
          }),
          catchError((message: string) => {
            this.notificationService.colorizeRed();
            this.notificationService.setMessage(message);
            return of({ type: '[Account] Update Account Error' });
          })
        )
      })
    )
  });

  pswdAccount$ = createEffect(() => {
    return this.actions.pipe(
      ofType(pswdAccount),
      withLatestFrom(this.store.select(selectToken)),
      map(([action, token]) => ({ ...action.payload, token })),
      mergeMap(({ token, ...credentials }) => {
        return this.accountService.fetchPswdAccount(token, credentials).pipe(
          map(({ updatedId }) => {
            if (!updatedId) {
              throwError('No ID');
            }

            this.notificationService.colorizeGreen();
            this.notificationService.setMessage('Success !');
            return { type: '[Account] Pswd Account Success' };
          }),
          catchError((message: string) => {
            this.notificationService.colorizeRed();
            this.notificationService.setMessage(message);
            return of({ type: '[Account] Pswd Account Error' });
          })
        )
      })
    )
  });

  deleteAccount$ = createEffect(() => {
    return this.actions.pipe(
      ofType(deleteAccount),
      withLatestFrom(this.store.select(selectToken)),
      map(([action, token]) => token),
      mergeMap(token => {
        return this.accountService.fetchDeleteAccount(token).pipe(
          map(({ deletedId }) => {
            if (!deletedId) {
              throwError('No ID');
            }

            return resetAccount();
          }),
          tap(() => this.router.navigate(['/'])),
          catchError((message: string) => {
            this.notificationService.colorizeRed();
            this.notificationService.setMessage(message);
            return of({ type: '[Account] Delete Account Error' });
          })
        )
      })
    )
  });
 
  constructor(
    private actions: Actions,
    private store: Store<{ account: AccountReducer }>,
    private router: Router,
    private accountService: AccountService,
    private notificationService: NotificationService
  ) {}
}

import { Action } from '@ngrx/store';
import { accountReducer } from '../account.reducer';

describe('Account: Reducer', () => {
  const initialState = {
    login: '',
    email: '',
    firstName: '',
    lastName: '',
    gender: '',
    yearOld: 0,
    token: null
  };

  it("Should 'Set Token' Case Returns State", () => {
    expect(accountReducer(undefined, { type: '[Account] Set Token', payload: 'ABCDEF123456' } as Action)).toEqual({
      ...initialState,
      token: 'ABCDEF123456'
    });
  });

  it("Should 'Set Account' Case Returns State", () => {
    const payload = {
      login: 'Pickle',
      email: 'rick.sanchez@pm.me',
      firstName: 'Lorem',
      lastName: 'Ipsum',
      gender: 'M',
      yearOld: 70
    };

    expect(accountReducer(undefined, { type: '[Account] Set Account', payload } as Action)).toEqual({
      ...initialState,
      ...payload
    });
  });

  it("Should 'Reset Account' Case Returns Initial State", () => {
    const state = {
      login: 'Pickle',
      email: 'rick.sanchez@pm.me',
      firstName: 'Lorem',
      lastName: 'Ipsum',
      gender: 'M',
      yearOld: 70,
      token: 'ABCDEF123456'
    };

    expect(accountReducer(state, { type: '[Account] Reset Account' } as Action)).toEqual(initialState);
  });
});

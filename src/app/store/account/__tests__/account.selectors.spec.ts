import * as Selectors from '../account.selectors';

describe('Account: Selectors', () => {
  const initialState = {
    login: '',
    email: '',
    firstName: '',
    lastName: '',
    gender: '',
    yearOld: 0,
    token: null
  };

  const state = {
    account: {
      login: 'Pickle',
      email: 'rick.sanchez@pm.me',
      firstName: 'Rick',
      lastName: 'Sanchez',
      gender: 'M',
      yearOld: 70,
      token: 'ABCDEF123456'
    }
  };

  it("Should 'selectAccount' Returns State", () => {
    expect(Selectors.selectAccount({ account: initialState })).toEqual(initialState);
    expect(Selectors.selectAccount(state)).toEqual({
      login: 'Pickle',
      email: 'rick.sanchez@pm.me',
      firstName: 'Rick',
      lastName: 'Sanchez',
      gender: 'M',
      yearOld: 70,
      token: 'ABCDEF123456'
    });
  });

  it("Should 'selectLogin' Returns 'login'", () => {
    expect(Selectors.selectLogin({ account: initialState })).toBeFalsy();
    expect(Selectors.selectLogin(state)).toEqual('Pickle');
  });

  it("Should 'selectEmail' Returns 'email'", () => {
    expect(Selectors.selectEmail({ account: initialState })).toBeFalsy();
    expect(Selectors.selectEmail(state)).toEqual('rick.sanchez@pm.me');
  });

  it("Should 'selectFirstName' Returns 'firstName'", () => {
    expect(Selectors.selectFirstName({ account: initialState })).toBeFalsy();
    expect(Selectors.selectFirstName(state)).toEqual('Rick');
  });

  it("Should 'selectLastName' Returns 'lastName'", () => {
    expect(Selectors.selectLastName({ account: initialState })).toBeFalsy();
    expect(Selectors.selectLastName(state)).toEqual('Sanchez');
  });

  it("Should 'selectGender' Returns 'gender'", () => {
    expect(Selectors.selectGender({ account: initialState })).toBeFalsy();
    expect(Selectors.selectGender(state)).toEqual('M');
  });

  it("Should 'selectYearOld' Returns 'yearOld'", () => {
    expect(Selectors.selectYearOld({ account: initialState })).toBeFalsy();
    expect(Selectors.selectYearOld(state)).toEqual(70);
  });

  it("Should 'selectToken' Returns 'token'", () => {
    expect(Selectors.selectToken({ account: initialState })).toBeFalsy();
    expect(Selectors.selectToken(state)).toEqual('ABCDEF123456');
  });
});

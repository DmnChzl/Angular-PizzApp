import { createAction } from '@ngrx/store';
import { LoginAndPassword, Account, AccountWithPassword, Profile, Credentials } from '../../models/account.model';

enum Types {
  SetToken = '[Account] Set Token',
  SetAccount = '[Account] Set Account',
  ResetAccount = '[Account] Reset Account',
  LoginAccount = '[Account] Login Account',
  ReadAccount = '[Account] Read Account',
  RegisterAccount = '[Account] Register Account',
  LogoutAccount = '[Account] Logout Account',
  UpdateAccount = '[Account] Update Account',
  PswdAccount = '[Account] Pswd Account',
  DeleteAccount = '[Account] Delete Account'
}

export const setToken = createAction(Types.SetToken, (token: string | null) => ({ payload: token }));

export const setAccount = createAction(Types.SetAccount, (account: Partial<Account>) => ({ payload: account }));

export const resetAccount = createAction(Types.ResetAccount);

export const loginAccount = createAction(Types.LoginAccount, (account: LoginAndPassword) => ({ payload: account }));

export const readAccount = createAction(Types.ReadAccount, (token: string) => ({ payload: token }));

export const registerAccount = createAction(Types.RegisterAccount, (account: AccountWithPassword) => ({ payload: account }));

export const logoutAccount = createAction(Types.LogoutAccount);

export const updateAccount = createAction(Types.UpdateAccount, (account: Profile) => ({ payload: account }));

export const pswdAccount = createAction(Types.PswdAccount, (credentials: Credentials) => ({ payload: credentials }));

export const deleteAccount = createAction(Types.DeleteAccount);

import { createReducer, on, Action } from '@ngrx/store';
import * as AccountActions from './account.actions';
import { AccountReducer } from '../../models/account.model';

const initialState: AccountReducer = {
  login: '',
  email: '',
  firstName: '',
  lastName: '',
  gender: '',
  yearOld: 0,
  token: null
};

const reducer = createReducer(
  initialState,
  on(AccountActions.setToken, (state: AccountReducer, { payload }) => {
    return {
      ...state,
      token: payload
    };
  }),
  on(AccountActions.setAccount, (state: AccountReducer, { payload }) => {
    return {
      ...state,
      ...payload
    };
  }),
  on(AccountActions.resetAccount, () => ({
    login: '',
    email: '',
    firstName: '',
    lastName: '',
    gender: '',
    yearOld: 0,
    token: null
  }))
);

type AccountReducerOrUndefined = AccountReducer | undefined;

export function accountReducer(state: AccountReducerOrUndefined, action: Action) {
  return reducer(state, action);
}

import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, mergeMap, withLatestFrom } from 'rxjs/operators';
import { allPizzas, setPizzas } from './pizzas.actions';
import { selectPizzas } from './pizzas.selectors';
import { PizzasReducer } from '../../models/pizzas.model';
import { NotificationService } from '../../services/notification.service';
import { PizzasService } from '../../services/pizzas.service';
import { sortByKey } from '../../utils';
 
@Injectable()
export class PizzasEffect {

  allPizzas$ = createEffect(() => {
    return this.actions.pipe(
      ofType(allPizzas),
      withLatestFrom(this.store.select(selectPizzas)),
      map(([action, pizzas]) => ({ key: action.payload, pizzas })),
      mergeMap(({ key, pizzas }) => {
        if (pizzas.length === 0) {
          return this.pizzasService.fetchAllPizzas().pipe(
            map(allPizzas => {
              const sortedByLabel = allPizzas.sort(sortByKey(key));

              return setPizzas(sortedByLabel);
            })
          );
        }

        return of({ type: '[Pizzas] No More Pizza' });
      })
    )
  });
 
  constructor(private actions: Actions, private store: Store<{ pizzas: PizzasReducer }>, private pizzasService: PizzasService, private notificationService: NotificationService) {}
}

import { createSelector } from '@ngrx/store';
import { Pizza } from '../../models/pizzas.model';

export const selectPizzas = (state: { pizzas: Pizza[] }) => state.pizzas || [];

export const selectPizzaById = createSelector(
  selectPizzas,
  (pizzas: Pizza[], id: string) => pizzas.find(pizza => pizza.id === id)
);

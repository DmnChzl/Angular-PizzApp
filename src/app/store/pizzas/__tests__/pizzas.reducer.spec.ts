import { Action } from '@ngrx/store';
import { pizzasReducer } from '../pizzas.reducer';

describe('Pizzas: Reducer', () => {
  const initialState = [];

  it("Should 'Set Pizzas' Case Returns State", () => {
    const payload = [
      {
        id: 'ABCDEF123456',
        label: '4 Cheeses',
        items: ['Mozzarella', 'Goat Cheese', 'Reblochon', 'Gorgonzola'],
        price: 14.9
      }
    ];

    expect(pizzasReducer(undefined, { type: '[Pizzas] Set Pizzas', payload } as Action)).toEqual(payload);
  });

  it("Should 'Add Pizza' Case Returns State", () => {
    const payload = {
      id: 'ABCDEF123456',
      label: '4 Cheeses',
      items: ['Mozzarella', 'Goat Cheese', 'Reblochon', 'Gorgonzola'],
      price: 14.9
    };

    expect(pizzasReducer(undefined, { type: '[Pizzas] Add Pizza', payload } as Action)).toEqual([
      ...initialState,
      payload
    ]);
  });

  it("Should 'Up Pizza' Case Returns State", () => {
    const state = [
      {
        id: 'ABCDEF123456',
        label: '4 Cheeses',
        items: ['Mozzarella', 'Goat Cheese', 'Reblochon', 'Gorgonzola'],
        price: 14.9
      }
    ];

    const payload = {
      id: 'ABCDEF123456',
      label: '3 Cheeses',
      items: ['Mozzarella', 'Reblochon', 'Gorgonzola'],
      price: 9.9
    };

    expect(pizzasReducer(state, { type: '[Pizzas] Up Pizza', payload } as Action)).toEqual(
      state.map(val => val.id === 'ABCDEF123456' ? payload : val)
    );
  });

  it("Should 'Del Pizza' Case Returns State", () => {
    const state = [
      {
        id: 'ABCDEF123456',
        label: '4 Cheeses',
        items: ['Mozzarella', 'Goat Cheese', 'Reblochon', 'Gorgonzola'],
        price: 14.9
      }
    ];

    expect(pizzasReducer(state, { type: '[Pizzas] Del Pizza', payload: 'ABCDEF123456' } as Action)).toEqual(
      state.filter(val => val.id !== 'ABCDEF123456')
    );
  });

  it("Should 'Reset Pizzas' Case Returns Initial State", () => {
    const state = [
      {
        id: 'ABCDEF123456',
        label: '4 Cheeses',
        items: ['Mozzarella', 'Goat Cheese', 'Reblochon', 'Gorgonzola'],
        price: 14.9
      }
    ];

    expect(pizzasReducer(state, { type: '[Pizzas] Reset Pizzas' } as Action)).toEqual(initialState);
  });
});

import { createAction } from '@ngrx/store';
import { Pizza } from '../../models/pizzas.model';

enum Types {
  SetPizzas = '[Pizzas] Set Pizzas',
  AddPizza = '[Pizzas] Add Pizza',
  UpPizza = '[Pizzas] Up Pizza',
  DelPizza = '[Pizzas] Del Pizza',
  ResetPizzas = '[Pizzas] Reset Pizzas',
  AllPizzas = '[Pizzas] All Pizzas'
}

export const setPizzas = createAction(Types.SetPizzas, (pizzas: Pizza[]) => ({ payload: pizzas }));

export const addPizza = createAction(Types.AddPizza, (pizza: Pizza) => ({ payload: pizza }));

export const upPizza = createAction(Types.UpPizza, (pizza: Pizza) => ({ payload: pizza }));

export const delPizza = createAction(Types.DelPizza, (id: string) => ({ payload: id }));

export const resetPizzas = createAction(Types.ResetPizzas);

export const allPizzas = createAction(Types.AllPizzas, (key: string) => ({ payload: key }));

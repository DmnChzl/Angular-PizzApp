import { ActionReducer, Action } from '@ngrx/store';
import { merge, pick } from 'lodash';

const setSavedState = (state, storageKey: string) => {
  sessionStorage.setItem(storageKey, JSON.stringify(state));
};

const getSavedState = (storageKey: string) => {
  return JSON.parse(sessionStorage.getItem(storageKey));
};

export function storageMetaReducer(stateKeys: string[] = [], storageKey = 'store') {
  return function<S, A extends Action = Action>(reducer: ActionReducer<S, A>) {
    let onInit = true;
  
    return (state: S, action: A): S => {
      const nextState = reducer(state, action);
      
      if (onInit) {
        onInit = false;
        const savedState = getSavedState(storageKey);
        return merge(nextState, savedState);
      }
      
      const stateToSave = pick(nextState, stateKeys);
      setSavedState(stateToSave, storageKey);
      return nextState;
    };
  }
}

import { ValidatorFn, FormControl } from '@angular/forms';

/**
 * Custom Validator: Email
 *
 * @returns {ValidatorFn} Validator Function
 */
export const validEmail: ValidatorFn = (control: FormControl) => {
  const regex: RegExp = /\S+@\S+\.\S+/;

  if (regex.test(control.value)) {
    return null;
  }

  return {
    'email': true
  };
}

/**
 * Custom Validator: Number
 *
 * @param {Number} min Min
 * @param {Number} max Max
 * @returns {ValidatorFn} Validator Function
 */
export function validNumber(min?: number, max?: number): ValidatorFn {
  return (control: FormControl) => {
    const value: number = control.value;

    if (min && value < min) {
      return {
        'min': {
          actual: value,
          required: min
        }
      };
    }

    if (max && value > max) {
      return {
        'max': {
          actual: value,
          required: max
        }
      };
    }
    
    return null;
  };
}
